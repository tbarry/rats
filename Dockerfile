# Docker file for RATS Static code analyzer 

FROM ubuntu:latest
MAINTAINER Thierno Barry "thierno.barry@thalesgroup.com"

VOLUME /shared

RUN apt-get update \
      && apt-get install -y \
      wget \
      cmake \
      gcc \
      autoconf \
      make

RUN wget https://github.com/libexpat/libexpat/releases/download/R_2_2_6/expat-2.2.6.tar.bz2 
# wget https://storage.googleapis.com/google-code-archive-downloads/v2/code.google.com/rough-auditing-tool-for-security/rats-2.4.tgz


